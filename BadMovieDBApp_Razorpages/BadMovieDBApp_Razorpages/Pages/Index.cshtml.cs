﻿using BadMovieDBApp_Razorpages.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BadMovieDBApp_Razorpages.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly BadMovieDBApp_Razorpages.Areas.Identity.Data.BadMovieDBContext _context;

        public IndexModel(ILogger<IndexModel> logger, BadMovieDBApp_Razorpages.Areas.Identity.Data.BadMovieDBContext context)
        {
            _context = context;
            _logger = logger;
        }


        // Show a list of movies on the bottom
        public IList<Movie> Movie { get; set; }
        public async Task OnGetAsync()
        {
            IQueryable<Movie> movies = (from m in _context.Movie orderby m.DateAdded descending select m).Take(3);
            Movie = await movies.ToListAsync();

        }
    }
}
