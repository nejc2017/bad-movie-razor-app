﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BadMovieDBApp_Razorpages.Areas.Identity.Data;
using BadMovieDBApp_Razorpages.Model;

namespace BadMovieDBApp_Razorpages.Pages.Movies
{
    public class IndexModel : PageModel
    {
        private readonly BadMovieDBApp_Razorpages.Areas.Identity.Data.BadMovieDBContext _context;

        public IndexModel(BadMovieDBApp_Razorpages.Areas.Identity.Data.BadMovieDBContext context)
        {
            _context = context;
        }

        //[BindProperty(SupportsGet = true)] is required for binding on HTTP GET requests.

        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }

        public IList<Movie> Movie { get;set; }
        public bool HasUserAdminRole { get; set; }
        public async Task OnGetAsync()
        {
            Movie = await _context.Movie
                .Include(u => u.User)
                .ToListAsync();

            //Search for movies
            //https://docs.microsoft.com/en-us/aspnet/core/tutorials/razor-pages/search?view=aspnetcore-5.0
            // get a list of movies
            var movies = from m in _context.Movie select m;

            if (!string.IsNullOrEmpty(SearchString))
            {
                movies = movies.Where(m => m.Title.Contains(SearchString));
            }
            // show search results in Movie object
            Movie = await movies.ToListAsync();


        }
    }
}
