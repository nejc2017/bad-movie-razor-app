﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BadMovieDBApp_Razorpages.Areas.Identity.Data;
using BadMovieDBApp_Razorpages.Model;

namespace BadMovieDBApp_Razorpages.Pages.Movies
{
    public class DetailsModel : PageModel
    {
        private readonly BadMovieDBApp_Razorpages.Areas.Identity.Data.BadMovieDBContext _context;

        public DetailsModel(BadMovieDBApp_Razorpages.Areas.Identity.Data.BadMovieDBContext context)
        {
            _context = context;
        }

        public Movie Movie { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Movie = await _context.Movie.FirstOrDefaultAsync(m => m.MovieID == id);

            if (Movie == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
