﻿using BadMovieDBApp_Razorpages.Areas.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BadMovieDBApp_Razorpages.Model
{
    public class Movie
    {
        public int MovieID { get; set; }

        [Required]
        [StringLength(60, ErrorMessage = "The title should be between {0} and {1} characters.", MinimumLength = 2)]
        public string Title { get; set; }

        [Required]
        [StringLength(1000, ErrorMessage = "The description should be shorter than 1000 characters.")]
        public string Description { get; set; }

        [Display(Name = "Date Released")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateReleased { get; set; }

        [Display(Name = "Date Added")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateAdded { get; set; }

        [Required]
        public string Genre { get; set; }

        [Display(Name = "Directed By")]
        public string DirectedBy { get; set; }

        [Display(Name = "IMDB Rating")]
        [Range(0, 10)]
        public double ImdbRating { get; set; }

        [Display(Name = "User Rating")]
        [Range(0, 10)]
        public double UserRating { get; set; }

        public string UserAdded { get; set; }
        public ApplicationUser User { get; set; }
    }
}
