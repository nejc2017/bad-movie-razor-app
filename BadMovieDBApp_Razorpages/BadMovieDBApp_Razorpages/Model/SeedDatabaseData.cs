﻿using BadMovieDBApp_Razorpages.Areas.Identity;
using BadMovieDBApp_Razorpages.Areas.Identity.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BadMovieDBApp_Razorpages.Model
{
    public class SeedDatabaseData
    {
        //https://stackoverflow.com/questions/50785009/how-to-seed-an-admin-user-in-ef-core-2-1-0
        // password hashing https://stackoverflow.com/questions/62029280/seeding-users-and-roles-in-entity-framework-core-3


        public static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            if (userManager.FindByEmailAsync("admin@example.com").Result == null)
            {
                ApplicationUser adminUser = new ApplicationUser
                {
                    Email = "admin@example.com",
                    NormalizedEmail = "ADMIN@EXAMPLE.COM",
                    UserName = "admin@example.com",
                    NormalizedUserName = "ADMIN@EXAMPLE.COM",
                    PhoneNumber = "+38641000000",
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString(),
                    IsAdmin = true
                };
                // add password
                IdentityResult result = userManager.CreateAsync(adminUser, "Geslo123!").Result;
            }

            //add normal users / members

            //generate users with emails
            ApplicationUser user1 = new ApplicationUser
            {
                Email = "janez@example.com",
                NormalizedEmail = "JANEZ@EXAMPLE.COM",
                UserName = "janez@example.com",
                NormalizedUserName = "JANEZ@EXAMPLE.COM",
                PhoneNumber = "+38641000001",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString(),
                IsAdmin = false
            };
            ApplicationUser user2 = new ApplicationUser
            {
                Email = "peter@example.com",
                NormalizedEmail = "PETER@EXAMPLE.COM",
                UserName = "peter@example.com",
                NormalizedUserName = "PETER@EXAMPLE.COM",
                PhoneNumber = "+38641000002",
                EmailConfirmed = true,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString(),
                IsAdmin = false
            };

            var userList = new List<ApplicationUser>()
                {
                    user1,
                    user2
                };

            //check if they exist
            foreach (var usr in userList)
            {
                if (userManager.FindByEmailAsync(usr.Email).Result == null)
                {
                    //Create Async
                    IdentityResult resultAdded = userManager.CreateAsync(usr, "Geslo123!").Result;

                }
            }
        }

        public static void SeedMovies(IServiceProvider serviceProvider)
        {
            using (var context = new BadMovieDBContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<BadMovieDBContext>>()))
            {

                if (context.Movie.Any())
                {
                    return;   // DB already has movies
                }

                context.Movie.AddRange(
                    new Movie
                    {
                        Title = "The Room",
                        Description = "Johnny is a successful bank executive who lives quietly in a San Francisco townhouse with his fiancée, Lisa. One day, putting aside any scruple, she seduces Johnny's best friend, Mark. From there, nothing will be the same again.",
                        Genre = "Drama",
                        DirectedBy = "Tommy Wiseau"
                    },

                    new Movie
                    {
                        Title = "Birdemic: Shock and Terror",
                        Description = "A horde of mutated birds descends upon the quiet town of Half Moon Bay, California. With the death toll rising, Two citizens manage to fight back, but will they survive Birdemic?",
                        Genre = "Drama",
                        DirectedBy = "James Nguyen"
                    },

                    new Movie
                    {
                        Title = "Ghostbusters 2",
                        Description = "ok",
                        Genre = "Comedy",
                        DirectedBy = "Ivan Reitman"
                    },

                    new Movie
                    {
                        Title = "Fateful Findings",
                        Description = "A computer-scientist/novelist reunites with his childhood friend, hacks into government databases, and faces the dire and fateful consequences of the mystical actions he obtained as a child.",
                        Genre = "Drama",
                        DirectedBy = "Neil Breen"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
