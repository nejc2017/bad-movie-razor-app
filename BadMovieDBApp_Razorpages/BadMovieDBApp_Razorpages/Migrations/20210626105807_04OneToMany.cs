﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BadMovieDBApp_Razorpages.Migrations
{
    public partial class _04OneToMany : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ApplicationUserID",
                table: "Movie",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ApplicationUserID",
                table: "Movie");
        }
    }
}
