﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BadMovieDBApp_Razorpages.Migrations
{
    public partial class _02MovieModelAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    MovieID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 60, nullable: false),
                    Description = table.Column<string>(maxLength: 1000, nullable: false),
                    DateReleased = table.Column<DateTime>(nullable: false),
                    DateAdded = table.Column<DateTime>(nullable: false),
                    Genre = table.Column<string>(nullable: false),
                    DirectedBy = table.Column<string>(nullable: true),
                    ImdbRating = table.Column<double>(nullable: false),
                    UserRating = table.Column<double>(nullable: false),
                    UserAdded = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.MovieID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Movie");
        }
    }
}
