﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BadMovieDBApp_Razorpages.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using BadMovieDBApp_Razorpages.Model;

namespace BadMovieDBApp_Razorpages.Areas.Identity.Data
{
    public class BadMovieDBContext : IdentityDbContext<ApplicationUser>
    {
        public BadMovieDBContext(DbContextOptions<BadMovieDBContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

        }

        public DbSet<BadMovieDBApp_Razorpages.Model.Movie> Movie { get; set; }
    }
}
