﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace BadMovieDBApp_Razorpages.Areas.Identity
{
    public static class IdentityExtensions
    {
        // admin role claim / extension method
        // https://stackoverflow.com/questions/39125347/how-to-get-claim-inside-asp-net-core-razor-view
        //https://stackoverflow.com/questions/38846816/how-to-get-custom-property-value-of-the-applicationuser-in-the-asp-net-mvc-5-vie
        public static string GetSpecificClaim(this ClaimsIdentity claimsIdentity, string claimType)
        {
            var claim = claimsIdentity.Claims.FirstOrDefault(x => x.Type == claimType);

            return (claim != null) ? claim.Value : string.Empty;
        }
    }
}
