﻿using System;
using BadMovieDBApp_Razorpages.Areas.Identity.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(BadMovieDBApp_Razorpages.Areas.Identity.IdentityHostingStartup))]
namespace BadMovieDBApp_Razorpages.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            //builder.ConfigureServices((context, services) =>
            //{
            //    services.AddDbContext<BadMovieDBContext>(options =>
            //        options.UseSqlServer(
            //            context.Configuration.GetConnectionString("BadMovieDBContextConnection")));

            //    services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true)
            //        .AddEntityFrameworkStores<BadMovieDBContext>();
            //});
        }
    }
}