﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BadMovieDBApp_Razorpages.Areas.Identity
{
    public class ApplicationUser : IdentityUser
    {
        public string FavoriteQuote { get; set; }
        public bool IsAdmin { get; set; }
        public ICollection<Model.Movie> Movies { get; set; }


    }
}
