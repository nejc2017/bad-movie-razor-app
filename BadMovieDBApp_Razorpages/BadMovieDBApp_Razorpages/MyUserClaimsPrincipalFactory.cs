﻿using BadMovieDBApp_Razorpages.Areas.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BadMovieDBApp_Razorpages
{
    public class MyUserClaimsPrincipalFactory : UserClaimsPrincipalFactory<ApplicationUser>
    {
		// Additional claims
		//https://forums.asp.net/t/2173703.aspx?+Identity+Use+current+user+s+attributes+in+CSHTML
		public MyUserClaimsPrincipalFactory(
			UserManager<ApplicationUser> userManager,
			IOptions<IdentityOptions> optionsAccessor)
			: base(userManager, optionsAccessor) { }
		protected override async Task<ClaimsIdentity> GenerateClaimsAsync(ApplicationUser user)
		{
			var identity = await base.GenerateClaimsAsync(user);
			identity.AddClaim(new Claim("AdminRole", user.IsAdmin.ToString()));
			return identity;
		}
	}
}