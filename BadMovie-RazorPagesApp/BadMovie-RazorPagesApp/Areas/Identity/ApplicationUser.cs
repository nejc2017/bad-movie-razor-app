﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BadMovie_RazorPagesApp.Areas.Identity
{
    public class ApplicationUser : IdentityUser
    {

        // define if the user should have administrative privileges
        public string FavoriteQuote { get; set; }
        public bool IsAdmin { get; set; }
    }
}
