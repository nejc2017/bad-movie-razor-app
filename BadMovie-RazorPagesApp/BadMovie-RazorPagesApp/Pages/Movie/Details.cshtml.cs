﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BadMovie_RazorPagesApp.Data;
using BadMovie_RazorPagesApp.Model;

namespace BadMovie_RazorPagesApp.Pages.Movie
{
    public class DetailsModel : PageModel
    {
        private readonly BadMovie_RazorPagesApp.Data.ApplicationDbContext _context;

        public DetailsModel(BadMovie_RazorPagesApp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Model.Movie Movie { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Movie = await _context.Movie.FirstOrDefaultAsync(m => m.ID == id);

            if (Movie == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
