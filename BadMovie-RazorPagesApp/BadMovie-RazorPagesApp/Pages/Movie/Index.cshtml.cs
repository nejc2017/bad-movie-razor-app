﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BadMovie_RazorPagesApp.Data;
using BadMovie_RazorPagesApp.Model;

namespace BadMovie_RazorPagesApp.Pages.Movie
{
    public class IndexModel : PageModel
    {
        private readonly BadMovie_RazorPagesApp.Data.ApplicationDbContext _context;

        public IndexModel(BadMovie_RazorPagesApp.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Model.Movie> Movie { get;set; }

        public async Task OnGetAsync()
        {
            Movie = await _context.Movie.ToListAsync();
        }
    }
}
